package com.Gifts.Repositories;

import com.Gifts.Entity.ComponentType;
import org.springframework.data.repository.CrudRepository;

public interface ComponentTypeRepository extends CrudRepository<ComponentType, Integer> {
}
