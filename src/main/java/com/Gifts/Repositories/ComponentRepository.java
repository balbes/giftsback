package com.Gifts.Repositories;

import com.Gifts.Entity.Component;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ComponentRepository extends CrudRepository<Component, Integer> {

    Component findById(int id);
    List<Component> findAll();
}
