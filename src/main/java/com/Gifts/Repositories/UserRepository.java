package com.Gifts.Repositories;

import com.Gifts.Entity.PermissionEnum;
import com.Gifts.Entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findUserById(int id);

    User findUserByUsername(String username);

    Boolean existsUserByUsername(String username);
}
