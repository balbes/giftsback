package com.Gifts.Repositories;

import com.Gifts.Entity.Order;
import com.Gifts.Entity.Status;
import com.Gifts.Entity.StatusEnum;
import com.Gifts.Entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {

    Order findById(int id);

    List<Order> findAllByStatus(Status status);

    List<Order> findAllByUser(User user);
}
