package com.Gifts.Repositories;

import com.Gifts.Entity.Status;
import com.Gifts.Entity.StatusEnum;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<Status, Integer> {
    Status findByName(StatusEnum statusEnum);
}
