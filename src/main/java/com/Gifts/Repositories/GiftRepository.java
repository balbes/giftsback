package com.Gifts.Repositories;

import com.Gifts.Entity.Gift;
import com.Gifts.Entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GiftRepository extends CrudRepository<Gift, Integer> {

    Gift findGiftById(int id);

    List<Gift> findAllByUser(User user);
}
