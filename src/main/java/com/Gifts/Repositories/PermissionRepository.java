package com.Gifts.Repositories;

import com.Gifts.Entity.Permission;
import com.Gifts.Entity.PermissionEnum;
import org.springframework.data.repository.CrudRepository;

public interface PermissionRepository extends CrudRepository<Permission, Integer> {

    Permission findPermissionById(int id);

    Permission findPermissionByName(PermissionEnum permissionEnum);
}
