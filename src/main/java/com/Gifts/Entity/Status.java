package com.Gifts.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "statuses")
public class Status extends BaseEntity {

    @Enumerated(value = EnumType.STRING)
    @Column(name = "name")
    private StatusEnum name;

    @OneToMany(mappedBy = "status")
    private List<Order> orders;

    public Status() {
    }

    public Status(StatusEnum name) {
        this.name = name;
    }

    public StatusEnum getName() {
        return name;
    }

    public void setName(StatusEnum name) {
        this.name = name;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
