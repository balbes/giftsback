package com.Gifts.Entity;

import javax.persistence.*;

@Entity
@Table(name = "permissions")
public class Permission extends BaseEntity {

    @Enumerated(value = EnumType.STRING)
    @Column(name = "name")
    private PermissionEnum name;

    public Permission() {
    }

    public Permission(PermissionEnum name) {
        this.name = name;
    }

    public PermissionEnum getName() {
        return name;
    }

    public void setName(PermissionEnum name) {
        this.name = name;
    }
}
