package com.Gifts.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "components")
public class Component extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "component_type_id")
    private ComponentType componentType;

    @ManyToMany
    @JoinTable(name = "gift_components",
                joinColumns = @JoinColumn(name = "component_id"),
                inverseJoinColumns = @JoinColumn(name = "gift_id"))
    private List<Gift> gifts;


    public Component() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public List<Gift> getGifts() {
        return gifts;
    }

    public void setGifts(List<Gift> gifts) {
        this.gifts = gifts;
    }
}
