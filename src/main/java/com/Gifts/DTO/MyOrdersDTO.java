package com.Gifts.DTO;

import javax.xml.crypto.Data;
import java.util.Date;

public class MyOrdersDTO {
    private int id;
    private String order;
    private Date time;
    private String status;
    private Double price;

    public MyOrdersDTO() {
    }

    public MyOrdersDTO(int id, String order, Date time, String status, Double price) {
        this.id = id;
        this.order = order;
        this.time = time;
        this.status = status;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
