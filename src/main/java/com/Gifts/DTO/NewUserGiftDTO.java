package com.Gifts.DTO;

import com.Gifts.Entity.ComponentType;

import java.util.List;

public class NewUserGiftDTO {
    private List<Integer> ingredientsId;
    private String username;

    public NewUserGiftDTO() {
    }

    public NewUserGiftDTO(List<Integer> ingredientsId, String username) {
        this.ingredientsId = ingredientsId;
        this.username = username;
    }

    public List<Integer> getIngredientsId() {
        return ingredientsId;
    }

    public void setIngredientsId(List<Integer> ingredientsId) {
        this.ingredientsId = ingredientsId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

