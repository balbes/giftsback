package com.Gifts.DTO;


import com.Gifts.Entity.ComponentType;

public class ChosenIngredients{
    private int id;
    private String title;
    private String type;
    private Double price;

    public ChosenIngredients() {
    }

    public ChosenIngredients(int id, String title, String type, Double price) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
