package com.Gifts.DTO;

import java.util.List;

public class MakeOrderDTO {
    private String customerAddres;
    private String customerNumber;
    private String customerFio;
    private String username;
    private Double price;
    private List<Integer> giftsId;

    public MakeOrderDTO() {
    }

    public MakeOrderDTO(String customerAddres, String customerNumber, String customerFio, String username, Double price, List<Integer> giftsId) {
        this.customerAddres = customerAddres;
        this.customerNumber = customerNumber;
        this.customerFio = customerFio;
        this.username = username;
        this.price = price;
        this.giftsId = giftsId;
    }

    public String getCustomerAddres() {
        return customerAddres;
    }

    public void setCustomerAddres(String customerAddres) {
        this.customerAddres = customerAddres;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerFio() {
        return customerFio;
    }

    public void setCustomerFio(String customerFio) {
        this.customerFio = customerFio;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Integer> getGiftsId() {
        return giftsId;
    }

    public void setGiftsId(List<Integer> giftsId) {
        this.giftsId = giftsId;
    }
}
