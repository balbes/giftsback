package com.Gifts.DTO;

import java.util.ArrayList;
import java.util.List;

public class AddNewGiftDTO {
    private List<Integer> ingredientsId = new ArrayList<>();
    private Double price;
    private String title;

    public AddNewGiftDTO() {
    }

    public AddNewGiftDTO(List<Integer> ingredientsId, Double price, String title) {
        this.ingredientsId = ingredientsId;
        this.price = price;
        this.title = title;
    }


    public List<Integer> getIngredientsId() {
        return ingredientsId;
    }

    public void setIngredientsId(List<Integer> ingredientsId) {
        this.ingredientsId = ingredientsId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
