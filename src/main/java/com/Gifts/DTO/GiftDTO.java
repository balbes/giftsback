package com.Gifts.DTO;

import com.Gifts.Entity.Component;

import java.util.List;

public class GiftDTO {
    private int id;
    private String image;
    private String title;
    private List<Component> components;
    private String description;
    private Double price;

    public GiftDTO() {
    }

    public GiftDTO(int id, String image, String title, List<Component> components, String description, Double price) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.components = components;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
