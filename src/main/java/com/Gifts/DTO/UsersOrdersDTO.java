package com.Gifts.DTO;

import java.util.Date;

public class UsersOrdersDTO {
    private int id;
    private String userFio;
    private String userNumber;
    private String userAddress;
    private String order;
    private Date time;
    private String status;
    private Double price;

    public UsersOrdersDTO() {
    }

    public UsersOrdersDTO(int id, String userFio, String userNumber, String userAddress, String order, Date time, String status, Double price) {
        this.id = id;
        this.userFio = userFio;
        this.userNumber = userNumber;
        this.userAddress = userAddress;
        this.order = order;
        this.time = time;
        this.status = status;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserFio() {
        return userFio;
    }

    public void setUserFio(String userFio) {
        this.userFio = userFio;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }
}
