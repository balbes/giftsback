package com.Gifts.Controllers;

import com.Gifts.DTO.AddNewGiftDTO;
import com.Gifts.DTO.GiftDTO;
import com.Gifts.DTO.NewUserGiftDTO;
import com.Gifts.Services.GiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/gifts")
public class GiftController {

    @Autowired
    private GiftService giftService;

    @GetMapping("/getDefaultGifts")
    public List<Map<String, Object>> getDefaultGifts() {
        return giftService.getDefaultGifts();
    }

    @PostMapping("/newUserGift")
    public ResponseEntity<?> createNewUserGift(@RequestBody NewUserGiftDTO newUserGiftDTO) {
        return giftService.createNewUserGift(newUserGiftDTO);
    }

    @PostMapping("/addNewGift")
    public ResponseEntity<?> addNewGift(@RequestBody AddNewGiftDTO addNewGiftDTO){
        return giftService.addNewGift(addNewGiftDTO);
    }

    @GetMapping("/{username}")
    public List<Map<String, Object>> getGifts(@PathVariable String username) {
        return giftService.getGifts(username);
    }
}
