package com.Gifts.Controllers;

import com.Gifts.Entity.Permission;
import com.Gifts.Entity.PermissionEnum;
import com.Gifts.Entity.User;
import com.Gifts.Forms.RegistrationForm;
import com.Gifts.Jwt.ResponseMessage;
import com.Gifts.Repositories.PermissionRepository;
import com.Gifts.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/register"})
public class RegistrationController {

    private UserRepository userRepository;
    private PermissionRepository permissionRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserRepository userRepository, PermissionRepository permissionRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.permissionRepository = permissionRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    public ResponseEntity<?> authenticateUser(@RequestBody RegistrationForm registrationForm) {

        if (userRepository.existsUserByUsername(registrationForm.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Username is already taken"), HttpStatus.BAD_REQUEST);
        }

        User user = new User(registrationForm.getUsername(), passwordEncoder.encode(registrationForm.getPassword()));
        Permission customer = permissionRepository.findPermissionByName(PermissionEnum.USER);

        user.setPermission(customer);
        userRepository.save(user);

        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }
}

