package com.Gifts.Controllers;

import com.Gifts.DTO.MakeOrderDTO;
import com.Gifts.DTO.MyOrdersDTO;
import com.Gifts.DTO.UsersOrdersDTO;
import com.Gifts.Services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PrePersist;
import java.util.List;

@RestController
@RequestMapping("api/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/getMy/{username}")
    public List<MyOrdersDTO> getMyOrders(@PathVariable String username) {
        return orderService.getMyOrders(username);
    }

    @GetMapping("/getUsersOrders")
    public List<UsersOrdersDTO> getUsersOrders() {
        return orderService.getUsersOrders();
    }

    @PostMapping("/submitOrder")
    public ResponseEntity<?> submitOrder(@RequestBody int id) {
        return orderService.submitOrder(id);
    }

    @GetMapping("/getUsersSubmitedOrders")
    public List<UsersOrdersDTO> getUsersSubmitedOrders(){
        return orderService.getUsersSubmitedOrders();
    }

    @PostMapping("/deliverOrder")
    public ResponseEntity<?> deliverOrder(@RequestBody int id) {
       return orderService.deliverOrder(id);
    }

    @GetMapping("/getUsersDeliveredOrders")
    public List<UsersOrdersDTO> getUsersDeliveredOrders(){
        return orderService.getUsersDeleeredOrders();
    }

    @PostMapping("/makeOrder")
    public ResponseEntity<?> makeOrder(@RequestBody MakeOrderDTO makeOrderDTO) {
        return orderService.makeOrder(makeOrderDTO);
    }


}
