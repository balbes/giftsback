package com.Gifts.Services;

import com.Gifts.DTO.MakeOrderDTO;
import com.Gifts.DTO.MyOrdersDTO;
import com.Gifts.DTO.UsersOrdersDTO;
import com.Gifts.Entity.*;
import com.Gifts.Jwt.ResponseMessage;
import com.Gifts.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private GiftRepository giftRepository;

    public List<MyOrdersDTO> getMyOrders(String username) {
        List<MyOrdersDTO> myOrders = new ArrayList<>();
        User user = userRepository.findUserByUsername(username);
        List<Order> orders = orderRepository.findAllByUser(user);
        String description = "";
        String status = "";

        for (Order order: orders){
            for (Gift gift: order.getGifts()){
                for (Component component: gift.getComponents()){
                    description += component.getName() + " ";
                }
                description += "||";
            }

            if (order.getStatus().getName() == StatusEnum.WAITING_OF_CONVERCATION)
                status = "Ждет подтверждения";
            if (order.getStatus().getName() == StatusEnum.IN_WAY)
                status = "В пути";
            if (order.getStatus().getName() == StatusEnum.DELIVERED)
                status = "Доставлено";

            myOrders.add(new MyOrdersDTO(order.getId(), description, order.getCreatedAt(), status, order.getPrice()));
        }

        return myOrders;
    }

    public List<UsersOrdersDTO> getUsersOrders() {
        List<UsersOrdersDTO> usersOrders = new ArrayList<>();
        List<Order> orders = orderRepository.findAllByStatus(statusRepository.findByName(StatusEnum.WAITING_OF_CONVERCATION));
        String description = "";

        for (Order order: orders){
            for (Gift gift: order.getGifts()){
                for (Component component: gift.getComponents()){
                    description += component.getName() + " ";
                }
                description += "||";
            }
            usersOrders.add(new UsersOrdersDTO(order.getId(), order.getCustomerFio(), order.getCustomerNumber(), order.getCustomerAddress(), description, order.getCreatedAt(), "Ждет подтверждения", order.getPrice()));
        }

        return usersOrders;
    }

    public ResponseEntity<?> submitOrder(int id) {
        Order order = orderRepository.findById(id);
        Status status = new Status();
        status.setOrders(order.getStatus().getOrders());
        status.setName(StatusEnum.IN_WAY);
        status.setId(2);
        order.setStatus(status);

        orderRepository.save(order);

        return new ResponseEntity<>(new ResponseMessage("OK"), HttpStatus.OK);
    }

    public List<UsersOrdersDTO> getUsersSubmitedOrders(){
        List<UsersOrdersDTO> usersOrders = new ArrayList<>();
        List<Order> orders = orderRepository.findAllByStatus(statusRepository.findByName(StatusEnum.IN_WAY));
        String description = "";

        for (Order order: orders){
            for (Gift gift: order.getGifts()){
                for (Component component: gift.getComponents()){
                    description += component.getName() + " ";
                }
                description += "||";
            }
            usersOrders.add(new UsersOrdersDTO(order.getId(), order.getCustomerFio(), order.getCustomerNumber(), order.getCustomerAddress(), description, order.getCreatedAt(), "В пути", order.getPrice()));
        }

        return usersOrders;
    }

    public ResponseEntity<?> deliverOrder(int id) {
        Order order = orderRepository.findById(id);
        Status status = new Status();
        status.setOrders(order.getStatus().getOrders());
        status.setName(StatusEnum.DELIVERED);
        status.setId(3);
        order.setStatus(status);

        orderRepository.save(order);

        return new ResponseEntity<>(new ResponseMessage("OK"), HttpStatus.OK);
    }

    public List<UsersOrdersDTO> getUsersDeleeredOrders() {
        List<UsersOrdersDTO> usersOrders = new ArrayList<>();
        List<Order> orders = orderRepository.findAllByStatus(statusRepository.findByName(StatusEnum.DELIVERED));
        String description = "";

        for (Order order: orders){
            for (Gift gift: order.getGifts()){
                for (Component component: gift.getComponents()){
                    description += component.getName() + " ";
                }
                description += "||";
            }
            usersOrders.add(new UsersOrdersDTO(order.getId(), order.getCustomerFio(), order.getCustomerNumber(), order.getCustomerAddress(), description, order.getCreatedAt(), "Доставлен", order.getPrice()));
        }

        return usersOrders;
    }

    public ResponseEntity<?> makeOrder(MakeOrderDTO makeOrderDTO) {
        User user = userRepository.findUserByUsername(makeOrderDTO.getUsername());
        Order order = new Order();
        List<Gift> giftList = new ArrayList<>();
        order.setCustomerAddress(makeOrderDTO.getCustomerAddres());
        order.setCustomerFio(makeOrderDTO.getCustomerFio());
        order.setCustomerNumber(makeOrderDTO.getCustomerNumber());
        order.setStatus(statusRepository.findByName(StatusEnum.WAITING_OF_CONVERCATION));
        order.setPrice(makeOrderDTO.getPrice());
        order.setUser(user);

        for (int gift: makeOrderDTO.getGiftsId()){
            giftList.add(giftRepository.findGiftById(gift));
        }
        order.setGifts(giftList);
        orderRepository.save(order);

        return new ResponseEntity<>(new ResponseMessage("OK"), HttpStatus.OK);
    }






}
