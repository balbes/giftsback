package com.Gifts.Services;

import com.Gifts.DTO.AddNewGiftDTO;
import com.Gifts.DTO.ChosenIngredients;
import com.Gifts.DTO.GiftDTO;
import com.Gifts.DTO.NewUserGiftDTO;
import com.Gifts.Entity.Component;
import com.Gifts.Entity.Gift;
import com.Gifts.Entity.PermissionEnum;
import com.Gifts.Entity.User;
import com.Gifts.Jwt.ResponseMessage;
import com.Gifts.Repositories.ComponentRepository;
import com.Gifts.Repositories.GiftRepository;
import com.Gifts.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GiftService {

    @Autowired
    private GiftRepository giftRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ComponentRepository componentRepository;

    public List<Map<String, Object>> getDefaultGifts() {
        List<Map<String, Object>> giftList = new ArrayList<>();
        List<Map<String, Object>> componentsList = new ArrayList<>();

        String description = "";

        User user = userRepository.findUserByUsername("creator");
        List<Gift> gifts = giftRepository.findAllByUser(user);

        for (Gift gift: gifts){
            Map<String, Object> item = new HashMap<>();
            for (Component component: gift.getComponents()){
                Map<String, Object> components = new HashMap<>();
                description += component.getName() + " ";
                components.put("id", component.getId());
                components.put("title", component.getName());
                componentsList.add(components);
            }

            item.put("id", gift.getId());
            item.put("image", gift.getImage());
            item.put("title", gift.getName());
            item.put("description", description);
            item.put("components", componentsList);
            item.put("price", gift.getPrice());
            description = "";
            giftList.add(item);
            componentsList.clear();
        }

        return giftList;
    }

    public ResponseEntity<?> createNewUserGift(NewUserGiftDTO newUserGiftDTO) {
        User user = userRepository.findUserByUsername(newUserGiftDTO.getUsername());
        Gift gift = new Gift();
        List<Component> componentList = new ArrayList<>();
        Double price = 0D;

        for (int component: newUserGiftDTO.getIngredientsId()){
            componentList.add(componentRepository.findById(component));
        }

        for (Component component: componentList){
            price += component.getPrice();
        }

        gift.setName("Пользовательский подарок");
        gift.setImage("[p");
        gift.setPrice(price);
        gift.setUser(user);
        gift.setComponents(componentList);

        giftRepository.save(gift);


        return new ResponseEntity<>(new ResponseMessage("OK"), HttpStatus.OK);
    }

    public ResponseEntity<?> addNewGift(AddNewGiftDTO addNewGiftDTO){
        User user = userRepository.findUserByUsername("creator");
        Gift gift = new Gift();
        List<Component> componentList = new ArrayList<>();

        for (int component: addNewGiftDTO.getIngredientsId()){
            componentList.add(componentRepository.findById(component));
        }

        gift.setName(addNewGiftDTO.getTitle());
        gift.setImage("[p");
        gift.setPrice(addNewGiftDTO.getPrice());
        gift.setUser(user);
        gift.setComponents(componentList);

        giftRepository.save(gift);

        return new ResponseEntity<>(new ResponseMessage("OK"), HttpStatus.OK);
    }

    public List<Map<String, Object>> getGifts(String username) {
        List<Map<String, Object>> giftList = new ArrayList<>();
        List<Map<String, Object>> componentsList = new ArrayList<>();

        String description = "";

        User user = userRepository.findUserByUsername(username);
        List<Gift> gifts = giftRepository.findAllByUser(user);

        for (Gift gift: gifts){
            Map<String, Object> item = new HashMap<>();
            for (Component component: gift.getComponents()){
                Map<String, Object> components = new HashMap<>();
                description += component.getName() + " ";
                components.put("id", component.getId());
                components.put("title", component.getName());
                componentsList.add(components);
            }

            item.put("id", gift.getId());
            item.put("image", gift.getImage());
            item.put("title", gift.getName());
            item.put("description", description);
            item.put("components", componentsList);
            item.put("price", gift.getPrice());
            description = "";
            giftList.add(item);
            componentsList.clear();
        }

        return giftList;
    }

}
