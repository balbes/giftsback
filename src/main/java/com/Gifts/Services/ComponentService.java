package com.Gifts.Services;

import com.Gifts.Entity.Component;
import com.Gifts.Repositories.ComponentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ComponentService {

    @Autowired
    private ComponentRepository componentRepository;

    public List<Map<String, Object>> getAll() {
        List<Map<String, Object>> components = new ArrayList<>();

        List<Component> componentList = componentRepository.findAll();

        for (Component component: componentList){
            Map<String, Object> item = new HashMap<>();
            item.put("id", component.getId());
            item.put("title", component.getName());
            item.put("type", component.getComponentType().getName());
            item.put("price", component.getPrice());
            components.add(item);
        }

        return components;
    }
}
