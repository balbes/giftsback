package com.Gifts.Forms;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChangePasswordForm {

    public String email;
    public String oldPassword;
    public String newPassword;
}
