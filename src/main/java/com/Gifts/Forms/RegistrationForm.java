package com.Gifts.Forms;

import lombok.Data;

@Data
public class RegistrationForm {

    private String username;
    private String password;

}
